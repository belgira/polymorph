package com.intevalue;

import com.intevalue.base.Action;
import com.intevalue.base.impl.Jumping;
import com.intevalue.base.impl.Running;
import com.intevalue.base.impl.Walking;

public class Application {

	public static void main(String[] args) {
		Action jumping = new Jumping();
		Action running = new Running();
		Action walking = new Walking();
		
		System.out.println(jumping.getAction());
		System.out.println(running.getAction());
		System.out.println(walking.getAction());
		
	}

}
